# Attract Mode #
A simple clock screensaver, but with a retro twist - the clock not only looks like it was drawn on an Atari 2600, the foreground and background also change colour every 7 seconds to prevent burn-in, like the Attract Mode of the Atari 2600 game console and home computers. Choice between a 'floating' (changing position every 7 seconds) and a static clock, and you can also choose blinking separators if you want. Binary supplied in the downloads section. MIT License.

Building
--------
Project is made in Xcode 10.3. Currently builds for macOS 10.12 and up.

Version history
---------------
### 1.1 (build 4, current): ###
* Changed a few bits here and there, so that the clock cannot be moved in 'floating mode' halfway during a full second, because that looks silly;
* Now also immediately reflects changes in the settings in the preview;
* Fixed a bug that prevented 'blacking out' the screen on display sleep.

### 1.0 (build 1): ###
* Initial release.
