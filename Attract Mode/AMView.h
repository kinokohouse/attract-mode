//
//  AttractModeView.h
//  Attract Mode Screen Saver
//
//  Created by Petros Loukareas on 12/09/2019.
//  Copyright © 2019 Petros Loukareas. All rights reserved.
//

#import <ScreenSaver/ScreenSaver.h>

@interface AMView : ScreenSaverView

@property (nonatomic, strong) IBOutlet NSPanel *prefsWindow;
@property (weak) IBOutlet NSButton *blinkCheckBox;
@property (weak) IBOutlet NSButton *modeCheckBox;

@property (assign) CGFloat factor;
@property (assign) CGFloat blocks;
@property (assign) CGFloat xPosition;
@property (assign) CGFloat yPosition;
@property (assign) BOOL blink;
@property (assign) BOOL mode;
@property (assign) BOOL toggle;
@property (assign) BOOL vertical;
@property (assign) BOOL screenDidSleep;
@property (assign) BOOL screenIsAsleep;
@property (assign) NSUInteger counter;
@property (assign) NSUInteger hh;
@property (assign) NSUInteger mm;
@property (assign) NSUInteger ss;
@property (strong) NSColor *fgcolor;
@property (strong) NSColor *bgcolor;

@end
