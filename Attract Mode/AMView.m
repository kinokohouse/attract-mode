//
//  AttractModeView.m
//  Attract Mode Screen Saver
//
//  Created by Petros Loukareas on 12/09/2019.
//  Copyright © 2019 Petros Loukareas. All rights reserved.
//

#import "AMView.h"

static NSString * const bundleIdent = @"nl.kinoko-house.attract2600";

@implementation AMView

- (instancetype)initWithFrame:(NSRect)frame isPreview:(BOOL)isPreview {
    self = [super initWithFrame:frame isPreview:isPreview];
    ScreenSaverDefaults *defaults = [ScreenSaverDefaults defaultsForModuleWithName:bundleIdent];
    [defaults registerDefaults:[NSDictionary dictionaryWithObjectsAndKeys:
                                @NO, @"blink",
                                @YES, @"mode",
                                nil]];
    _blink = [defaults boolForKey:@"blink"];
    _mode = [defaults boolForKey:@"mode"];
    if (self) {
        if ([self bounds].size.height > [self bounds].size.width) {
            _vertical = YES;
            _factor = [self bounds].size.width / 68;
            _blocks = [self bounds].size.height / _factor;
        } else {
            _vertical = NO;
            _factor = [self bounds].size.height / 50;
            _blocks = [self bounds].size.width / _factor;
        }
        if (_mode) {
            if (!_vertical) {
                _xPosition = SSRandomFloatBetween(1.0f * _factor, (_blocks - 56) * _factor);
                _yPosition = SSRandomFloatBetween(1.0f * _factor, 42.0f * _factor);
            } else {
                _xPosition = SSRandomFloatBetween(1.0f * _factor, 12.0f * _factor);
                _yPosition = SSRandomFloatBetween(1.0f * _factor, (_blocks - 9) * _factor);
            }
        } else {
            if (!_vertical) {
                _xPosition = ((_blocks - 54) / 2) * _factor;
                _yPosition = 21 * _factor;
            } else {
                _xPosition = ([self bounds].size.width - 54 * _factor) / 2;
                _yPosition = ((_blocks - 8) / 2) * _factor;
            }
        }
        _toggle = NO;
        _counter = 0;
        _fgcolor = [NSColor colorWithRed:SSRandomFloatBetween(0.0f, 0.5f)
                                   green:SSRandomFloatBetween(0.0f, 0.5f)
                                    blue:SSRandomFloatBetween(0.0f, 0.5f)
                                   alpha:1.0f];
        _bgcolor = [NSColor colorWithRed:SSRandomFloatBetween(0.0f, 0.5f)
                                   green:SSRandomFloatBetween(0.0f, 0.5f)
                                    blue:SSRandomFloatBetween(0.0f, 0.5f)
                                   alpha:1.0f];
        _screenDidSleep = NO;
        _screenIsAsleep = NO;
        [self setAnimationTimeInterval:1 / 2.0];
    }
    [[[NSWorkspace sharedWorkspace] notificationCenter] addObserver:self selector:@selector(screenFellAsleep) name:NSWorkspaceScreensDidSleepNotification object:nil];
    return self;
}

- (void)startAnimation {
    [super startAnimation];
}

- (void)stopAnimation {
    [super stopAnimation];
}

- (void)drawRect:(NSRect)rect {
    if (_screenIsAsleep) return;
    if (_screenDidSleep) {
        [super drawRect:rect];
        [[NSColor blackColor] set];
        NSRectFill([self bounds]);
        [self setAnimationTimeInterval:60];
        _screenIsAsleep = YES;
        return;
    }
    CGFloat xPos;
    CGFloat yPos;
    [super drawRect:rect];
    if (!_mode) {
        [_fgcolor set];
    } else {
        [_bgcolor set];
    }
    NSRectFill([self bounds]);
    if (!_mode) {
        NSRect theRect = [self bounds];
        theRect.origin.x += 2 * _factor;
        theRect.origin.y += 2 * _factor;
        theRect.size.width -= 4 * _factor;
        theRect.size.height -= 4 * _factor;
        [_bgcolor set];
        NSRectFill(theRect);
    }
    [_fgcolor set];
    if (!_toggle) {
        NSDate *date = [NSDate date];
        NSCalendar *cal = [NSCalendar currentCalendar];
        NSDateComponents *comp = [cal components:(NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond) fromDate:date];
        _hh = [comp hour];
        _mm = [comp minute];
        _ss = [comp second];
        _counter++;
        if (_counter > 6) {
            _fgcolor = [NSColor colorWithRed:SSRandomFloatBetween(0.0f, 0.5f)
                                       green:SSRandomFloatBetween(0.0f, 0.5f)
                                        blue:SSRandomFloatBetween(0.0f, 0.5f)
                                       alpha:1.0f];
            _bgcolor = [NSColor colorWithRed:SSRandomFloatBetween(0.0f, 0.5f)
                                       green:SSRandomFloatBetween(0.0f, 0.5f)
                                        blue:SSRandomFloatBetween(0.0f, 0.5f)
                                       alpha:1.0f];
            if (_mode) {
                if (!_vertical) {
                    _xPosition = SSRandomFloatBetween(1.0f * _factor, (_blocks - 56) * _factor);
                    _yPosition = SSRandomFloatBetween(1.0f * _factor, 42.0f * _factor);
                } else {
                    _xPosition = SSRandomFloatBetween(1.0f * _factor, 12.0f * _factor);
                    _yPosition = SSRandomFloatBetween(1.0f * _factor, (_blocks - 9) * _factor);
                }
            }
            _counter = 0;
        }
    }
    if (!_mode) {
        if (!_vertical) {
            _xPosition = ((_blocks - 54) / 2) * _factor;
            _yPosition = 21 * _factor;
        } else {
            _xPosition = ([self bounds].size.width - 54 * _factor) / 2;
            _yPosition = ((_blocks - 8) / 2) * _factor;
        }
        [_fgcolor set];
        NSRectFill([self bounds]);
        NSRect theRect = [self bounds];
        theRect.origin.x += 2 * _factor;
        theRect.origin.y += 2 * _factor;
        theRect.size.width -= 4 * _factor;
        theRect.size.height -= 4 * _factor;
        [_bgcolor set];
        NSRectFill(theRect);
    } else {
        [_bgcolor set];
        NSRectFill([self bounds]);
    }
    [_fgcolor set];
    xPos = _xPosition;
    yPos = _yPosition;
    NSBezierPath *timePath = [NSBezierPath bezierPath];
    [timePath moveToPoint:NSMakePoint(xPos, yPos)];
    
    NSString *h = [NSString stringWithFormat:@"%lu", (unsigned long)_hh];
    if ([h length] < 2) {
        [timePath appendBezierPath:[self drawAnyNumber:0 atPoint:NSMakePoint(xPos, yPos)]];
        xPos += (8 * _factor);
    }
    for (int i = 0; i < [h length]; i++) {
        NSString *hx = [h substringWithRange:NSMakeRange(i, 1)];
        NSUInteger hw = [hx integerValue];
        [timePath appendBezierPath:[self drawAnyNumber:hw atPoint:NSMakePoint(xPos, yPos)]];
        xPos += (8 * _factor);
    }
    if (!_toggle || !_blink) {
        [timePath appendBezierPath:[self drawAnyNumber:10 atPoint:NSMakePoint(xPos, yPos)]];
    }
    xPos += (4 * _factor);
    
    NSString *m = [NSString stringWithFormat:@"%lu", (unsigned long)_mm];
    if ([m length] < 2) {
        [timePath appendBezierPath:[self drawAnyNumber:0 atPoint:NSMakePoint(xPos, yPos)]];
        xPos += (8 * _factor);
    }
    for (int i = 0; i < [m length]; i++) {
        NSString *mx = [m substringWithRange:NSMakeRange(i, 1)];
        NSUInteger mw = [mx integerValue];
        [timePath appendBezierPath:[self drawAnyNumber:mw atPoint:NSMakePoint(xPos, yPos)]];
        xPos += (8 * _factor);
    }
    if (!_toggle || !_blink) {
        [timePath appendBezierPath:[self drawAnyNumber:10 atPoint:NSMakePoint(xPos, yPos)]];
    }
    xPos += (4 * _factor);
    
    NSString *s = [NSString stringWithFormat:@"%lu", (unsigned long)_ss];
    if ([s length] < 2) {
        [timePath appendBezierPath:[self drawAnyNumber:0 atPoint:NSMakePoint(xPos, yPos)]];
        xPos += (8 * _factor);
    }
    for (int i = 0; i < [s length]; i++) {
        NSString *sx = [s substringWithRange:NSMakeRange(i, 1)];
        NSUInteger sw = [sx integerValue];
        [timePath appendBezierPath:[self drawAnyNumber:sw atPoint:NSMakePoint(xPos, yPos)]];
        xPos += (8 * _factor);
    }
    
    [timePath fill];
    
    _toggle = !_toggle;
}

- (void)animateOneFrame {
    [self setNeedsDisplay:YES];
}

- (BOOL)hasConfigureSheet {
    return YES;
}

- (NSWindow *)configureSheet {
    if (!_prefsWindow)
    {
        if (![[NSBundle bundleWithIdentifier:@"nl.kinoko-house.attract-mode"] loadNibNamed:@"AMPrefs" owner:self topLevelObjects:nil]) {
            NSLog(@"CONFIG SHEET: Failed to load configure sheet.");
            NSBeep();
            return nil;
        }
    }
    ScreenSaverDefaults *defaults = [ScreenSaverDefaults defaultsForModuleWithName:bundleIdent];
    [defaults registerDefaults:[NSDictionary dictionaryWithObjectsAndKeys:
                                @NO, @"blink",
                                @YES, @"mode",
                                nil]];
    [_blinkCheckBox setState:[defaults boolForKey:@"blink"]];
    [_modeCheckBox setState:[defaults boolForKey:@"mode"]];
    return _prefsWindow;
}

- (NSBezierPath *)drawAnyNumber:(NSUInteger)number atPoint:(NSPoint)thePoint {
    NSBezierPath *path = [NSBezierPath bezierPath];
    [path setWindingRule:NSWindingRuleEvenOdd];
    [path setLineWidth:1.0f];
    [path moveToPoint:thePoint];
    
    switch(number) {
            
        case 0:
            [path relativeLineToPoint:NSMakePoint(0 * _factor, 7 * _factor)];
            [path relativeLineToPoint:NSMakePoint(6 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, -7 * _factor)];
            [path closePath];
            [path relativeMoveToPoint:NSMakePoint(2 * _factor, 1 * _factor)];
            [path relativeLineToPoint:NSMakePoint(2 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, 5 * _factor)];
            [path relativeLineToPoint:NSMakePoint(-2 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, -5 * _factor)];
            [path closePath];
            [path relativeMoveToPoint:NSMakePoint(-2 * _factor, -1 * _factor)];
            break;
            
        case 1:
            [path relativeLineToPoint:NSMakePoint(0 * _factor, 1 * _factor)];
            [path relativeLineToPoint:NSMakePoint(2 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, 4 * _factor)];
            [path relativeLineToPoint:NSMakePoint(-2 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, 1 * _factor)];
            [path relativeLineToPoint:NSMakePoint(2 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, 1 * _factor)];
            [path relativeLineToPoint:NSMakePoint(2 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, -6 * _factor)];
            [path relativeLineToPoint:NSMakePoint(2 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, -1 * _factor)];
            [path closePath];
            break;
            
        case 2:
            [path relativeLineToPoint:NSMakePoint(0 * _factor, 4 * _factor)];
            [path relativeLineToPoint:NSMakePoint(4 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, 2 * _factor)];
            [path relativeLineToPoint:NSMakePoint(-4 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, 1 * _factor)];
            [path relativeLineToPoint:NSMakePoint(6 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, -4 * _factor)];
            [path relativeLineToPoint:NSMakePoint(-4 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, -2 * _factor)];
            [path relativeLineToPoint:NSMakePoint(4 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, -1 * _factor)];
            [path closePath];
            break;
            
        case 3:
            [path relativeLineToPoint:NSMakePoint(0 * _factor, 1 * _factor)];
            [path relativeLineToPoint:NSMakePoint(4 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, 2 * _factor)];
            [path relativeLineToPoint:NSMakePoint(-4 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, 1 * _factor)];
            [path relativeLineToPoint:NSMakePoint(4 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, 2 * _factor)];
            [path relativeLineToPoint:NSMakePoint(-4 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, 1 * _factor)];
            [path relativeLineToPoint:NSMakePoint(6 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, -7 * _factor)];
            [path closePath];
            break;
            
        case 4:
            [path relativeMoveToPoint:NSMakePoint(4 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, 3 * _factor)];
            [path relativeLineToPoint:NSMakePoint(-4 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, 4 * _factor)];
            [path relativeLineToPoint:NSMakePoint(2 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, -3 * _factor)];
            [path relativeLineToPoint:NSMakePoint(2 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, 3 * _factor)];
            [path relativeLineToPoint:NSMakePoint(2 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, -7 * _factor)];
            [path closePath];
            break;
            
        case 5:
            [path relativeLineToPoint:NSMakePoint(0 * _factor, 1 * _factor)];
            [path relativeLineToPoint:NSMakePoint(4 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, 2 * _factor)];
            [path relativeLineToPoint:NSMakePoint(-4 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, 4 * _factor)];
            [path relativeLineToPoint:NSMakePoint(6 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, -1 * _factor)];
            [path relativeLineToPoint:NSMakePoint(-4 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, -2 * _factor)];
            [path relativeLineToPoint:NSMakePoint(4 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, -4 * _factor)];
            [path closePath];
            break;
            
        case 6:
            [path relativeLineToPoint:NSMakePoint(0 * _factor, 7 * _factor)];
            [path relativeLineToPoint:NSMakePoint(6 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, -1 * _factor)];
            [path relativeLineToPoint:NSMakePoint(-4 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, -2 * _factor)];
            [path relativeLineToPoint:NSMakePoint(4 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, -4 * _factor)];
            [path closePath];
            [path relativeMoveToPoint:NSMakePoint(2 * _factor, 1 * _factor)];
            [path relativeLineToPoint:NSMakePoint(2 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, 2 * _factor)];
            [path relativeLineToPoint:NSMakePoint(-2 * _factor, 0 * _factor)];
            [path closePath];
            [path relativeMoveToPoint:NSMakePoint(-2 * _factor, -1 * _factor)];
            break;
            
        case 7:
            [path relativeMoveToPoint:NSMakePoint(4 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, 6 * _factor)];
            [path relativeLineToPoint:NSMakePoint(-4 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, 1 * _factor)];
            [path relativeLineToPoint:NSMakePoint(6 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, -7 * _factor)];
            [path closePath];
            break;
            
        case 8:
            [path relativeLineToPoint:NSMakePoint(0 * _factor, 7 * _factor)];
            [path relativeLineToPoint:NSMakePoint(6 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, -7 * _factor)];
            [path closePath];
            [path relativeMoveToPoint:NSMakePoint(2 * _factor, 1 * _factor)];
            [path relativeLineToPoint:NSMakePoint(2 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, 2 * _factor)];
            [path relativeLineToPoint:NSMakePoint(-2 * _factor, 0 * _factor)];
            [path closePath];
            [path relativeMoveToPoint:NSMakePoint(0 * _factor, 3 * _factor)];
            [path relativeLineToPoint:NSMakePoint(2 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, 2 * _factor)];
            [path relativeLineToPoint:NSMakePoint(-2 * _factor, 0 * _factor)];
            [path closePath];
            [path relativeMoveToPoint:NSMakePoint(-2 * _factor, -4 * _factor)];
            break;
            
        case 9:
            [path relativeLineToPoint:NSMakePoint(0 * _factor, 1 * _factor)];
            [path relativeLineToPoint:NSMakePoint(4 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, 2 * _factor)];
            [path relativeLineToPoint:NSMakePoint(-4 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, 4 * _factor)];
            [path relativeLineToPoint:NSMakePoint(6 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, -7 * _factor)];
            [path closePath];
            [path relativeMoveToPoint:NSMakePoint(2 * _factor, 4 * _factor)];
            [path relativeLineToPoint:NSMakePoint(2 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, 2 * _factor)];
            [path relativeLineToPoint:NSMakePoint(-2 * _factor, 0 * _factor)];
            [path closePath];
            [path relativeMoveToPoint:NSMakePoint(-2 * _factor, -4 * _factor)];
            break;
            
        default:
            [path relativeMoveToPoint:NSMakePoint(0 * _factor, 1 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, 2 * _factor)];
            [path relativeLineToPoint:NSMakePoint(2 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, -2 * _factor)];
            [path closePath];
            [path relativeMoveToPoint:NSMakePoint(0 * _factor, 3 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, 2 * _factor)];
            [path relativeLineToPoint:NSMakePoint(2 * _factor, 0 * _factor)];
            [path relativeLineToPoint:NSMakePoint(0 * _factor, -2 * _factor)];
            [path closePath];
            [path relativeMoveToPoint:NSMakePoint(-2 * _factor, -4 * _factor)];
            break;
            
    }
    return path;
}

- (IBAction)OKClicked:(id)sender {
    _blink = [_blinkCheckBox state];
    _mode = [_modeCheckBox state];
    ScreenSaverDefaults *defaults = [ScreenSaverDefaults defaultsForModuleWithName:bundleIdent];
    [defaults setBool:_blink forKey:@"blink"];
    [defaults setBool:_mode forKey:@"mode"];
    [defaults synchronize];
    _counter = 7;
    _toggle = 0;
    [[NSApplication sharedApplication] endSheet:_prefsWindow];
}

- (void)screenFellAsleep {
    _screenDidSleep = YES;
}
@end
